<?php


class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 's9622031_beejee';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 's9622031_beejee';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'E3*LmIRA';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}