
<div>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Beejee test</a>
            </div>
            <?if (!$auth):?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login/"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            <?else:?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login/logout/"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </ul>
            <?endif?>
        </div>
    </nav>
</div>


<div>
    <table id="task" align="center" class="table table-striped table-bordered"></table>
</div>

<div class="container">

        <form method="POST" action="/main/add/">
            <div class="form-group">
                <label for="name_input">Name</label>
                <input type="name" name="name" class="form-control" id="name_input" placeholder="Ваше имя" required>
                <div class="valid-feedback">
                    Looks good!
                </div>
            </div>
            <div class="form-group">
                <label for="input_email">Email address</label>
                <input type="email" name="email" class="form-control" id="input_email" placeholder="Ваш e-mail" required>
                <div class="valid-feedback">
                    Looks good!
                </div>
                <div class="invalid-feedback">
                     E-mail address entered incorrectly.
                </div>
            </div>

            <div class="form-group">
                <label for="task_textarea">Task</label>
                <textarea name = "task" class="form-control" id="task_textarea" required></textarea>
                <div class="valid-feedback">
                    Looks good!
                </div>
            </div>

            <? if($auth):?>
            <div class="form-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input name = "status" class="form-check-input" type="checkbox"> Задача выполнена
                    </label>
                </div>
            </div>
            <?endif?>
            <button type="submit" id="add_task"class="btn btn-primary float-right">Внести задачу</button>
        </form>

</div>

<script>
$(document).ready(function() {
    $('#task').DataTable( {
        data: <?=($data)?>,
        columns: [
        {title: "Name", data: "name" },
        { title: "E-mail", data: "email" },
        { title: "Task", data: "task", class:"no-sort"},
        { title: "Status", data: "status"},

            <? if($auth):?>
                { title: "Mark", data: "mark", class:"no-sort edit"}
            <? else:?>
                { title: "Mark", data: "mark", class:"no-sort"}
            <?endif?>
        ],
        lengthMenu: [ 3 ],
        } );

        var table = $('#task').DataTable();
        $('#task tbody td.edit').on('click',  function () {
            var tr = $(this).closest('tr');
            var row = table .row( tr ).data();
            window.location.replace("/main/detail/?id="+row['id']);

        });
});
</script>

