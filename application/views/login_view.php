<div class="wrapper fadeInDown">
    <div id="formContent">

        <div class="fadeIn first">
            <img src="/images/login.png" id="icon" alt="User Icon" />
        </div>

        <!-- Login Form -->
        <form method="post" action="/login/active/">
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="login" required>
            <input type="text" id="password" class="fadeIn third" name="password" placeholder="password" required>
            <input type="submit" class="fadeIn fourth" value="Log In">
        </form>


    </div>
</div>