<? if ($auth):?>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <div class="container">

                <form method="POST" action ='/main/update/'>
                    <div class="form-group">
                        <label for="name_input">Name</label>
                        <input type="name" name="name" class="form-control" id="name_input" value = "<?echo $data['name']?>">
                    </div>
                    <div class="form-group">
                        <label for="input_email">Email address</label>
                        <input type="email" name="email" class="form-control" id="input_email" value = "<?echo $data['email']?>">
                    </div>

                    <div class="form-group">
                        <label for="task_textarea">Task</label>
                        <textarea name = "task" class="form-control" id="task_textarea"><?echo $data['task']?></textarea>
                    </div>


                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input name = "status" class="form-check-input" type="checkbox"   <? if ($data['status']=='Закрыта'):?>checked<?endif?>> Задача выполнена
                            </label>
                        </div>
                    </div>

                    <input type="hidden" name="id"  value = "<?echo $data['id']?>">
                    <button type="submit" class="btn btn-primary float-right">Внести правки</button>
                </form>

        </div>
    </div>
</div>
<?else:?>
    <p>Пройдите регистрацию</p>>
<?endif?>








