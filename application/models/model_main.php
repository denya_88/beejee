<?php


class Model_Main extends Model
{
    public $id;
    public $name;
    public $email;
    public $task;
    public $status;
    public $mark;

    public function get_data()
    {
        $db = static::getDB();
        $row = $db->query('SELECT * FROM task');
        return json_encode($row->fetchAll(PDO::FETCH_ASSOC));
    }


    public function add_date()
    {


        $count = 0;

        $data = [
            ':name' => $this->name,
            ':email' => $this->email,
            ':task' => $this->task,
            ':status' => $this->status,
            ':mark' => $this->mark,
        ];


        $sql = 'INSERT INTO task (name, email, task, status, mark) VALUES (:name, :email, :task, :status, :mark)';
        $pdo = $this->getDB();

        if (!is_null($pdo)) {
            $sth = $pdo->prepare($sql);
            $count = $sth->execute($data);

            $pdo = null;

        }

        return $count;
    }

    public function update_date()
    {
        $count = 0;

        $data = [
            ':id' => $this->id,
            ':name' => $this->name,
            ':email' => $this->email,
            ':task' => $this->task,
            ':status' => $this->status,
            ':mark' => $this->mark,
        ];


        $sql = 'UPDATE task SET  name=:name, email=:email, task=:task, status=:status, mark=:mark WHERE id=:id';
        $pdo = $this->getDB();

        if (!is_null($pdo)) {
            $sth = $pdo->prepare($sql);
            $count = $sth->execute($data);
            $pdo = null;
        }
        return $count;
    }


    public function detail_date()
    {
        $data = [
            ':id' => $this->id,
        ];

        $sql = 'SELECT * FROM task WHERE id=:id';
        $pdo = $this->getDB();

        if (!is_null($pdo)) {
            $sth = $pdo->prepare($sql);
            $sth->execute($data);
            $item = $sth->fetch();
            $pdo = null;
        }
        return $item;
    }

}