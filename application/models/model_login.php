<?php


class Model_Login extends Model
{

    public $username;
    public $password;


    public function login()
    {
        $auth = false;

        if ($this->username == 'admin'
            and $this->password == '123') {
                    session_start();
                    $_SESSION['admin'] = true;
                    session_write_close();
                    $auth = true;
        }


        return $auth;
    }

    public function logout()
    {
        session_start();
        session_destroy();
        $auth = false;
        return $auth;
    }

    public function isauth() {
        $auth = false;
        session_start();
        if(isset($_SESSION['admin'])) {
            $auth = $_SESSION['admin'];
        }
        session_write_close();

        return $auth;
    }

}