<?php
include('application/models/model_main.php');

class Controller_Login extends Controller
{   function __construct()
{
    $this->login = new Model_Login();
    $this->main = new Model_Main();
    $this->view = new View();
}

	function action_index()
	{
		$this->view->generate('login_view.php', 'template_view.php');
	}

    function action_active()
    {

        $this->login->username = $_POST["login"];
        $this->login->password = $_POST["password"];
        $auth = $this->login->login();

        $data = $this->main->get_data();
        $this->view->generate('main_view.php', 'template_view.php', $data, $auth);

    }

    function action_logout()
    {

        $auth = $this->login->logout();
        $data = $this->main->get_data();
        $this->view->generate('main_view.php', 'template_view.php',$data,$auth);

    }




}
