<?php
include ('application/models/model_login.php');
class Controller_Main extends Controller
{

    function __construct()
    {
        $this->main = new Model_Main();
        $this->login = new Model_Login();
        $this->view = new View();
    }

	function action_index()
	{
        $data = $this->main->get_data();
        $auth = $this->login->isauth();
        $this->view->generate('main_view.php', 'template_view.php',$data,$auth);
	}


    function action_add()
    {
        $this->main->name = htmlentities($_POST['name']);
        $this->main->email = htmlentities($_POST['email']);
        $this->main->task = htmlentities($_POST['task']);
        if (!isset($_POST['status']))
            $this->main->status = 'В работе';
        else{
            $this->main->status = 'Закрыта';
            $this->main->mark = 'Отредактировано администратором';
        }

        if (!isset($this->main->mark)) $this->main->mark = ' ';
        $this->main->add_date();

        header('location:/');

    }


    function action_detail()
    {
        $this->main->id = $_GET['id'];
        $detail = $this->main->detail_date();
        $auth = $this->login->isauth();
        $this->view->generate('detail_view.php', 'template_view.php',$detail,$auth);
    }


    function action_update()
    {

        $this->main->id = $_POST['id'];
        $this->main->name = htmlentities($_POST['name']);
        $this->main->email = htmlentities($_POST['email']);
        $this->main->task = htmlentities($_POST['task']);
        if (!isset($_POST['status']))
            $this->main->status = 'В работе';
        else
            $this->main->status = 'Закрыта';
        $this->main->mark = 'Отредактировано администратором';
        $this->main->update_date();

        header('location:/');
    }
}